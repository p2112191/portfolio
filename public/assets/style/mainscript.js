const body = document.getElementsByTagName('body')[0];
const overlay = document.getElementsByClassName('overlay')[0];
const langues = document.getElementsByClassName('languages')[0];

const handlettering = document.getElementsByClassName('handlettering')[0];
const nav = document.getElementsByTagName('nav')[0];
const html = document.getElementsByTagName('html')[0];

const competencesConteneur = document.getElementsByClassName("competences")[0];
const semestreConteneurs = document.getElementsByClassName("semestre");
const compdesem = document.getElementsByClassName("comp");


/*
semestreConteneurs[1].addEventListener('mouseover', hoversem2);
semestreConteneurs[1].addEventListener('mouseout', nolongerhoversem2);

semestreConteneurs[0].addEventListener('mouseover', hoversem1);
semestreConteneurs[0].addEventListener('mouseout', nolongerhoversem1);

semestreConteneurs[2].addEventListener('mouseover', hoversem3);
semestreConteneurs[2].addEventListener('mouseout', nolongerhoversem3);

semestreConteneurs[3].addEventListener('mouseover', hoversem4);
semestreConteneurs[3].addEventListener('mouseout', nolongerhoversem4);

semestreConteneurs[4].addEventListener('mouseover', hoversem5);
semestreConteneurs[4].addEventListener('mouseout', nolongerhoversem5);

*/
//semestreConteneurs[5].addEventListener('mouseover', hoversem6);
//semestreConteneurs[5].addEventListener('mouseout', nolongerhoversem6);

langues.addEventListener('mouseover', hoverLangues);
langues.addEventListener('mouseout', nolongerhoverLangues);


handlettering.addEventListener('mouseover', hoverhandlettering);
handlettering.addEventListener('mouseout', nolongerhoverhandlettering);

body.addEventListener('click', clickAnalysis);


var modalView = false;

function clickAnalysis(evt){
    var target = evt.target;
    
    if(target.className === 'project' || target.parentNode.className === 'project'){
        modalView = true;
        
        modal = openModalView(target);
    }
    else if (modalView === true && !target.classList.contains("modal") && !target.parentNode.classList.contains("layout") && !target.parentNode.classList.contains("projetdescription") && !target.parentNode.classList.contains("comp")){
        modalView = false;
        closeModalView(modal);

    }
    else if(modalView === true ){
        console.log(target.classList.contains("images"));
        //target.classList.
    }
    //console.log(target.parentNode);
}


function openModalView(target) {
    overlay.classList.add("visible");

    if (target.className != 'project') {
        var id = target.parentNode.id;
    } else {
        var id = target.id;
    }

    var modal = document.getElementsByClassName(id)[0];
    modal.classList.add('visible');
    
    return modal;
}

function closeModalView(modal) {
    overlay.classList.remove('visible');
    modal.classList.remove('visible');

}

function hoverLangues(evt) {
    langues.children.item(1).classList.remove('invisible');
    langues.children.item(0).classList.remove('visible')
    langues.children.item(0).classList.add('invisible');
    langues.children.item(1).classList.add('languagedisplay');
}

function nolongerhoverLangues(evt) {
    langues.children.item(0).classList.remove('invisible');
    langues.children.item(0).classList.add('visible');
    langues.children.item(1).classList.remove('languagedisplay');
    langues.children.item(1).classList.add('invisible');
    
}

function hoverhandlettering(evt) {
    handlettering.children.item(1).classList.remove('invisible');
    handlettering.children.item(0).classList.remove('visible')
    handlettering.children.item(0).classList.add('invisible');
    handlettering.children.item(1).classList.add('visible','handletteringimg');
}

function nolongerhoverhandlettering(evt) {
    handlettering.children.item(0).classList.remove('invisible');
    handlettering.children.item(0).classList.add('visible');
    handlettering.children.item(1).classList.remove('visible', 'handletteringimg');
    handlettering.children.item(1).classList.add('invisible');
}


/*

function hideComp(cpt) {
    const children = semestreConteneurs[cpt].children;
    var j = 0;
    for (const child of children) {
        if (j!=0) {
            child.classList.remove('visible');
            child.classList.add('invisible');
            for (const iterator of child.children) {
                iterator.classList.remove('visible');
                iterator.classList.add('invisible');
                
            } 
        } 
        j++;
    }
}


function displayComp(cpt) {
    
    const children = semestreConteneurs[cpt].children;
    //console.log(children);
    for (const child of children) {
        child.classList.remove('invisible');
        child.classList.add('visible');
        for (const iterator of child.children) {
            iterator.classList.add('visible');
        }  
    }
}

function setRegularfor(cpt) {
    for (let i = 0; i < 5; i++) {
        const fils = semestreConteneurs[i].children;
        if (i!=cpt) {
            semestreConteneurs[i].classList.remove('defaultsem');
            semestreConteneurs[i].classList.add('regular');            
        }

        var iterator = 0;
            for (const unfils of fils) {
                if (iterator!=0) {
                    unfils.classList.add("invisible");
                    for (const child of unfils.children) {
                        child.classList.add("invisible");
                    }
                }
               iterator++;
            }
    }
}

//const children = semestreConteneurs[0].children;

/*function displaychildren() {
    
    for (const child of children) {
        child.classList.remove('invisible');
        child.classList.add('visible');
       
        for (const iterator of child.children) {
            iterator.classList.add('visible');
        }  
    }

}

var open = false;



function hoversem1(evt) {
    setRegularfor(0);
    displayComp(0) ;
}

function nolongerhoversem1(evt) {
    hideComp(0);    
}

function hoversem2(evt) {
    setRegularfor(1);
    displayComp(1);
}

function nolongerhoversem2(evt) {
    hideComp(1);
}

function hoversem3(evt) {
    setRegularfor(2);
    displayComp(2) ;
}

function nolongerhoversem3(evt) {
    hideComp(2);    
}

function hoversem4(evt) {
    setRegularfor(3);
    displayComp(3);
}

function nolongerhoversem4(evt) {
    hideComp(3);
}

function hoversem5(evt) {
    setRegularfor(4);
    displayComp(4) ;
}

function nolongerhoversem5(evt) {
    hideComp(4);    
}
/*
function hoversem6(evt) {
    setRegularfor(5);
    displayComp(5);
}

function nolongerhoversem6(evt) {
    hideComp(5);
}
*/